//
//  videoCell.swift
//  NetflixClone
//
//  Created by Guneet on 2019-03-14.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit

class videoCell: UITableViewCell {

    @IBOutlet weak var episodeImage: UIImageView!
    @IBOutlet weak var episodeLabel: UILabel!

}
