//
//  ViewController.swift
//  NetflixClone
//
//  Created by Guneet on 2019-03-13.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    // MARK : Outlets.
    @IBOutlet weak var showCollection: UICollectionView!
    @IBOutlet weak var showCollection2: UICollectionView!
    
    
    //MARK: Variables
    let show = ["show1.png","show2.png","show3.png","show4.png","show5.png","show6.png"]
    let show2 = ["show7.png","show8.png","show9.png","show1.png","show5.png","show3.png"]
    var index = 0
    var image:String?
        override func viewDidLoad()
        {
        super.viewDidLoad()
        showCollection.delegate = self
        showCollection.dataSource = self
        showCollection.reloadData()
        showCollection2.delegate = self
        showCollection2.dataSource = self
        showCollection2.reloadData()
        self.view.addSubview(showCollection)
        self.view.addSubview(showCollection2)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return show.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)as! collectionCell
        
        if(collectionView == self.showCollection)
        {
            cell.showImage.image = UIImage(named: show[indexPath.row])
     
    }
        else if (collectionView == self.showCollection2)
        {
            //cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)as! collectionCell
            
            cell.showImage.image = UIImage(named: show2[indexPath.row])
           
        }
         return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        if (collectionView == self.showCollection)
        {
            image = self.show[index]
            performSegue(withIdentifier: "toVideo", sender: nil)
        }
        else if(collectionView == self.showCollection2)
        {
           
            image = self.show2[index]
            performSegue(withIdentifier: "toVideo", sender: nil)
        }
}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        let destinationVc = segue.destination as! videoMenuController
        
        destinationVc.image = image
        
    }
    
    
    

    
    
    
}
    


        
        





