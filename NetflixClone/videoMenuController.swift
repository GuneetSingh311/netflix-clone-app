//
//  videoMenuController.swift
//  NetflixClone
//
//  Created by Guneet on 2019-03-14.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit

class videoMenuController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var image:String?
    let episodes = ["Episode 1","Episode2","Episode3","Episode4","Episode5","Episode6","Episode7","Episode8","Episode9","Episode10"]
    
    
    @IBOutlet weak var episodeTable: UITableView!
    
    @IBOutlet weak var showImage: UIImageView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        episodeTable.delegate = self
        episodeTable.dataSource = self
        self.showImage.image = UIImage(named: image!)
        
}
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "episodes", for: indexPath) as!videoCell
        cell.episodeImage.image = UIImage(named: image!)
        cell.episodeLabel.text = episodes[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}
